package org.bitbucket.nhweston.mcknapsack

import org.bitbucket.nhweston.mcknapsack.Selectable.Combination
import org.bitbucket.nhweston.mcknapsack.Category._

import scala.collection.immutable.Queue

sealed abstract class Category[C, E] (val selectables: Seq[Selectable[E]], val numToSelect: Int) {

    lazy val culled: Seq[Selectable[E]] = {
        selectables.sortBy(e => -e.value).sift {
            (elem, acc, _, _) => acc.count(e => (e.value >= elem.value) && (e.cost <= elem.cost)) < numToSelect
        } ._1
    }

}

object Category {

    import language.implicitConversions

    case class ElemCategory[C, E] (
        label: C,
        override val selectables: Seq[Selectable[E]],
        override val numToSelect: Int
    ) extends Category[C, E] (selectables, numToSelect) {
        lazy val combinations: Seq[Seq[Selectable[E]]] = {
            culled C numToSelect
        }
    }

    case class MetaCategory[C, E] (
        categories: Seq[ElemCategory[C, E]],
        override val selectables: Seq[Combination[E]]
    ) extends Category[C, E] (selectables, 1)

    implicit class SeqImpl[T] (seq: Seq[T]) {

        /**
         * @param num   the number of elements to select in each combination.
         * @return      a `Seq` containing all combinations of `num` elements in this `Seq`.
         */
        def C (num: Int) : Seq[Seq[T]] = {
            if (seq.size < num || num <= 0) Nil
            else if (seq.size == num) Seq(seq)
            else if (num == 1) seq.map(Seq(_))
            else seq match {
                case head +: tail => (tail C num-1).map(head +: _) ++ (tail C num)
            }
        }

        /**
         * Partitions the set according to a predicate that may be parameterised with respect to the partial partitions
         * of previous elements as well as elements yet to be partitioned.
         *
         * @param predicate     a predicate with respect to the element in question, the partial partitions, and
         *                      elements yet to be partitioned (in that order).
         * @return              a sequence of elements for which the predicate was satisfied, paired with the sequence
         *                      of those for which the predicate was not satisfied.
         */
        def sift (predicate: (T, Seq[T], Seq[T], Seq[T]) => Boolean) : (Seq[T], Seq[T]) = {
            def aux (
                accepted: Seq[T],
                rejected: Seq[T],
                remaining: Seq[T],
            ) : (Seq[T], Seq[T]) = remaining match {
                case Nil => (accepted, rejected)
                case head +: tail =>
                    if (predicate(head, accepted, rejected, tail)) aux (
                        accepted :+ head,
                        rejected,
                        tail
                    )
                    else aux (
                        accepted,
                        rejected :+ head,
                        tail
                    )
            }
            aux (Queue(), Queue(), seq)
        }

    }

}
