package org.bitbucket.nhweston.mcknapsack

sealed abstract class Selectable[T] {

    val labels: Seq[T]
    val value: Int
    val cost: Int

}

object Selectable {

    case class Element[T] (label: T, override val value: Int, override val cost: Int) extends Selectable[T] {

        override lazy val labels: Seq[T] = Seq(label)

    }

    case class Combination[T] (sub: Seq[Element[T]]) extends Selectable[T] {

        override lazy val labels: Seq[T] = sub.flatMap(_.labels)
        override lazy val value: Int = sub.foldLeft(0)(_ + _.value)
        override lazy val cost: Int = sub.foldLeft(0)(_ + _.cost)

    }

}
