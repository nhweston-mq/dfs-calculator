package org.bitbucket.nhweston.mcknapsack

import org.bitbucket.nhweston.mcknapsack.Category.{ElemCategory, MetaCategory}
import org.bitbucket.nhweston.mcknapsack.Selectable.{Combination, Element}
import org.bitbucket.nhweston.mcknapsack.Knapsack._

case class Knapsack[C, E] (categories: Seq[ElemCategory[C, E]], budget: Int) {

    lazy val combCull: Seq[Map[C, Set[Element[E]]]] = {
        categories.binaryAggregate[MetaCategory[C, E]] (
            category => MetaCategory[C, E] (
                Seq.fill(category.numToSelect)(category),
                category.combinations.map {
                    comb => Combination (
                        comb.flatMap {
                            case elem @ Element(_, _, _) => Seq(elem)
                            case Combination(elems) => elems
                        }
                    )
                }
            ),
            (cat1, cat2) => MetaCategory (
                cat1.categories ++ cat2.categories,
                (cat1.culled pairs cat2.culled).map {
                    case (comb1 @ Combination(_), comb2 @ Combination(_)) => Combination(comb1.sub ++ comb2.sub)
                },
            )
        ) match {
            case MetaCategory(elemCategories, selections) => selections.map {
                selection => elemCategories.zip(selection.sub).foldLeft(Map[C, Set[Element[E]]]()) {
                    case (map, (ElemCategory(label, _, _), elem)) => map.get(label) match {
                        case Some(set) => map + (label -> (set + elem))
                        case None => map + (label -> Set(elem))
                    }
                }
            }
        }
    }

    lazy val result: Map[C, Set[E]] = combCull.filter {
        selection => selection.values.flatten.foldLeft(0) (_ + _.cost) <= budget
    } .head.mapValues {
        set => set.map(_.label)
    }

}

object Knapsack {

    import language.implicitConversions

    implicit class SeqImpl[T] (seq: Seq[T]) {

        def pairs (other: Seq[T]) : Seq[(T, T)] = seq.flatMap(t1 => other.map(t2 => (t1, t2)))

        def binaryAggregate[TT] (elemFn: T => TT, aggregateFn: (TT, TT) => TT) : TT = seq match {
            case Nil => throw new MatchError(Nil)
            case Seq(head) => elemFn(head)
            case _ => seq.splitAt(seq.size / 2) match {
                case (s1, s2) => aggregateFn (
                    s1.binaryAggregate(elemFn, aggregateFn),
                    s2.binaryAggregate(elemFn, aggregateFn)
                )
            }
        }

    }

}
