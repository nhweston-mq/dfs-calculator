package org.bitbucket.nhweston.dfscalc

import java.time.{Duration, Instant}

import org.bitbucket.nhweston.mcknapsack.Knapsack

object Main {

    val filePath: String = "data/players.csv"

    def main (args: Array[String]) : Unit = {
        var serial: Int = 1
        val t0: Instant = Instant.now
        val knapsack: Knapsack[String, String] = FromCSV(filePath)
        val t1: Instant = Instant.now
        knapsack.result.values.flatten.foreach(println)
        println(s"Completed in ${Duration.between(t0, t1).toMillis} ms.")
    }

}
