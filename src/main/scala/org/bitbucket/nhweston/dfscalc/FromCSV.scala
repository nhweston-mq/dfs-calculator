package org.bitbucket.nhweston.dfscalc

import org.bitbucket.nhweston.mcknapsack.Category.ElemCategory
import org.bitbucket.nhweston.mcknapsack.Knapsack
import org.bitbucket.nhweston.mcknapsack.Selectable.Element

import scala.io.Source

object FromCSV {

    val budget: Int = 60000

    case class PlayerData (
        firstName: String,
        lastName: String,
        team: String,
        salary: Int,
        position: String,
        averagePoints: Double,
        gamesPlayed: Int,
        form: Double,
        playingStatus: String
    )

    def apply (filePath: String) : Knapsack[String, String] = Knapsack (
        Source.fromFile(filePath).getLines().toSeq.drop(1).map { l =>
            l.split(",").toSeq match {
                case fn +: ln +: t +: s +: p +: ap +: gp +: f +: ps +: Nil  => PlayerData (
                    fn, ln, t, s.toInt, p, ap.toDouble, gp.toInt, f.toDouble, ps
                )
            }
        } .filter {
            p => &&&[PlayerData] (
                _.form > 0,
                _.averagePoints > 0,
                _.gamesPlayed >= 3,
                _.playingStatus == "Not Named"
            ) (p)
        } .partitionInto(_.position).toSeq.map {
            case (cat, seq) => ElemCategory (
                cat,
                seq.map { p =>
                    Element (
                        s"(${p.position}) ${p.firstName} ${p.lastName}",
                        (10 * math.sqrt(p.averagePoints * p.form)).toInt,
                        p.salary
                    )
                },
                cat match {
                    case "C" => 1
                    case _ => 2
                }
            )
        },
        budget
    )

    case class &&&[T] (predicates: (T => Boolean)*) extends (T => Boolean) {
        override def apply (arg: T) : Boolean = aux(arg, predicates)
        def aux (arg: T, predicates: Seq[T => Boolean]) : Boolean = predicates match {
            case Nil => true
            case head +: tail => if (head(arg)) aux(arg, tail) else false
        }
    }

    implicit class SeqImpl[T] (seq: Seq[T]) {
        def partitionInto[K] (toKeyFn: T => K) : Map[K, Seq[T]] = seq.foldLeft(Map[K, Seq[T]]()) {
            case (map, elem) =>
                val key: K = toKeyFn(elem)
                map.get(key) match {
                    case Some(partition) => map + (key -> (partition :+ elem))
                    case None => map + (key -> Seq(elem))
                }
        }
    }

}
