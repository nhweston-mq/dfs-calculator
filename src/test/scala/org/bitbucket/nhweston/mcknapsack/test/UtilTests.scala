package org.bitbucket.nhweston.mcknapsack.test

import org.scalatest.FunSuite

import org.bitbucket.nhweston.mcknapsack.Category._

class UtilTests extends FunSuite {

    test ("Empty Seq") {
        val seq = Seq()
        assert((seq C 0) == Nil)
        assert((seq C 1) == Nil)
        assert((seq C 8) == Nil)
    }

    test ("Singleton Seq") {
        val seq = Seq(0)
        assert((seq C 0) == Nil)
        assert((seq C 1) == Seq(Seq(0)))
        assert((seq C 2) == Nil)
        assert((seq C 8) == Nil)
    }

    test ("Three elements") {
        val seq = Seq(1, 2, 3)
        assert((seq C 0) == Nil)
        assert (
            (seq C 1) == Seq (
                Seq(1), Seq(2), Seq(3)
            )
        )
        assert (
            (seq C 2) == Seq (
                Seq(1, 2), Seq(1, 3), Seq(2, 3)
            )
        )
        assert ((seq C 3) == Seq(Seq(1, 2, 3)))
        assert ((seq C 4) == Nil)
        assert ((seq C 8) == Nil)
    }

}
